---
layout: post
title: "Latex Markdown Post"
date: 2016-03-24 15:32:14 -0300
categories: jekyll update
---

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>AH-BP-development</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
    .display.math{display: block; text-align: center; margin: 0.5rem auto;}
  </style>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="marketing_plan">Marketing plan</h1>
<h5 id="section"></h5>
<p>The marketing mix is the set of marketing tools that we will use to pursue our marketing objectives in the target. It refers to four broad levels of marketing decision, namely: product, price, promotion, and place. This is an overview of our marketing plan, since we are first focusing on building a first proof-of-concept of our technology.</p>
<h2 id="product-service-strategy-product">Product &amp; service strategy (Product)</h2>
<h3 id="product-design">Product design</h3>
<h5 id="section-1"></h5>
<p>The product has been described in chapter , and our positioning in chapter .</p>
<h3 id="product-assortment">Product assortment</h3>
<h5 id="section-2"></h5>
<p>At first we will propose two variants of habitats: a <strong>limited-edition luxury version</strong>, made in collaboration with internationally renowned artists and architects, and a <strong>premium version</strong>. Both variants will be based on the same technology, but the luxury version will allow for more customization and more choices for the finishing surfaces, and be sold at a higher price, enabling higher margins. The next release will be a <strong>high-density affordable version</strong>, with a lower price and a more adaptable technology (it will be the first variant that can be attached to other buildings).</p>
<h3 id="services">Services</h3>
<h5 id="section-3"></h5>
<p>Once at cruising speed, we plan to deliver a series of services to our clients, including:</p>
<ul>
<li><p><em>Habitat design and VR/AR visualization:</em> a configurator can be used to automate building design based on site conditions, customer needs and building codes. Once the building has been designed, it can be visualized in a photorealistic VR/AR visualizer.</p></li>
<li><p><em>Habitat selling and financing:</em> with the help of a financial third party, we provide cheaper and faster financing solutions directly to our clients.</p></li>
<li><p><em>Land acquisition service:</em> we will secure exclusive pieces of land that could fit particularly well with our habitats. The goal is to prepare the land in order to accelerate the assembly process. In the case our customers are looking for a land that we do not own, we will help them to select and acquire it.</p></li>
<li><p><em>Component manufacturing:</em> we manufacture each habitat component in automated assembly lines.</p></li>
<li><p><em>Transportation and assembly:</em> we transport the components of each habitat with electric semi-trailers from the factory to the assembly site. The habitat is assembled on site at high speed and with high precision using robots.</p></li>
<li><p><em>Furnishing process:</em> in collaboration with many third party furnishing companies, we help our customers to choose, place and visualize furniture in their habitats.</p></li>
<li><p><em>Free OTA software update:</em> we regularly update for free the software that runs our habitats by improving its performance, enhancing its security and adding new features.</p></li>
<li><p><em>Paid hardware upgrade:</em> as we release new versions of the technical drawers, electronics or other habitat components, customer can buy them to upgrade their existing habitat (if compatible). Customers that want to deeply modify the layout or configuration (such as adding or removing an extension, a level etc.) of their habitat can do it using the configurator, to visualize changes in AR. Such changes are made by the same robots that are used for the standard assembly. A free hardware service will be provided for people who want to make holes in their wall ceramic finishing surfaces.</p></li>
<li><p><em>Habitat maintenance:</em> we offer maintenance services such as facade and roof cleaning.</p></li>
<li><p><em>Warranty:</em> with every habitat, we offer a 10-year limited warranty on the building and a 5-year limited warranty on the techniques and electronics.</p></li>
<li><p><em>Energy system management:</em> we manage the electricity generation and storage, and interactions with the power grid for each habitat that does not come with the full self-sufficiency package. We provide free electricity to our customers as they do not have a contract with electricity distributors. When needed, we upgrade and maintain the photovoltaics and batteries free of charge.</p></li>
<li><p><em>Cloud services:</em> we provide all needed cloud services to habitats, including those required by data analysis, security, swarm and AI features. Personal data is end-to-end encrypted and at no time accessible or usable by Ark Habitat, except for opt-in extra features, for which we anonymize data using state-of-the-art differential privacy methods.</p></li>
<li><p><em>Habitat parts reuse and recycling:</em> when parts are broken or simply replaced following a hardware upgrade, we collect them and reuse or recycle them, in line with our sustainability goals.</p></li>
</ul>
<h5 id="section-4"></h5>
<p>All those services will not be available from the start. We will first focus on "Habitat design and VR/AR visualization", "Transportation and assembly", "Free OTA software update", "Warranty" and "Cloud services". Our goal is to provide a seamless experience to our customers that makes the purchase and acquisition of our habitats quick and easy.</p>
<h3 id="life-cycle-management">Life-cycle management</h3>
<h5 id="section-5"></h5>
<p>When upgrading or dismantling habitats, remaining components will be recycled into raw materials for future use. We are planning on implementing a closed loop recycling program, in which we use recycled materials to build new habitat components, reducing both our costs and carbon footprint. Recycling will be done in partnership with the help of specialized third parties.</p>
<h2 id="pricing-strategy-price">Pricing strategy (Price)</h2>
<h3 id="price-strategy">Price strategy</h3>
<h5 id="section-6"></h5>
<p>Different price strategies are to be used for the different steps of the master plan. One must keep in mind that the higher price will be offset by government incentives (where and when available), lower insurance costs, lower utility bills, lower maintenance costs etc.</p>
<h4 id="luxury-habitats-price-skimming">Luxury habitats – Price skimming</h4>
<h5 id="section-7"></h5>
<p>During the market entry phase, we will first apply the price skimming strategy by selling a limited amount of habitats with high profit. This will match our initial low production capacity while reimbursing the cost of investment in R&amp;D, allowing us to reach the break-even point in a shorter time.</p>
<h4 id="premium-habitats-target-pricing">Premium habitats – Target pricing</h4>
<h5 id="section-8"></h5>
<p>A target pricing strategy will then be applied, by calculating the selling price of premium habitats to produce a particular rate of return on investment for a specific volume of production, while keeping a premium image by making our products perceived as more reliable and desirable, and representing exceptional quality and distinction.</p>
<h4 id="high-density-habitats-contribution-margin-based-pricing">High-density habitats – Contribution margin-based pricing</h4>
<h5 id="section-9"></h5>
<p>The next, lower-end variants of habitats will follow a contribution margin-based pricing, by maximizing our operating income. This requires to know the relationship between our product’s price and the units that can be sold at that price.</p>
<h3 id="price-tactics">Price tactics</h3>
<h4 id="even-pricing">Even pricing</h4>
<h5 id="section-10"></h5>
<p>We are applying an even pricing tactic to create and maintain a relationship based on trust and transparency. This also fits our premium brand image, since even prices are associated to higher quality goods.</p>
<h4 id="price-lining">Price lining</h4>
<h5 id="section-11"></h5>
<p>Price lining will be used to differentiate the different product lines, and their associated quality level. Product lines will be introduced as mentioned in the master plan (see section ), starting with luxury habitats (high price), premium habitats (medium-high price) and high-density habitats (medium price).</p>
<h4 id="price-bundling">Price bundling</h4>
<h5 id="section-12"></h5>
<p>We will use price bundling by including many free services in an habitat purchase. The total all-inclusive price will be lower than the price of the habitat itself and the price of the services (which benefits the customer) and increases profits because we are selling supplementary products and services other than just a habitat (which benefits us).</p>
<h4 id="two-part-tariff">Two-part tariff</h4>
<h5 id="section-13"></h5>
<p>We will apply two-part tariff (TPT) to the price of an habitat itself, composed of a lump-sum fee as well as a per-surface-unit charge. This allows to capture the consumer surplus<a href="#fn1" class="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>. If we consider a rectangular habitat, of length <span class="math inline"><em>l</em></span> and width <span class="math inline"><em>w</em></span>, we know that the area is given by <span class="math inline"><em>l</em><em>w</em></span> and that the length of facade walls is given by <span class="math inline">2(<em>l</em> + <em>w</em>)</span>. It is easy to show that when <span class="math inline"><em>l</em></span> and <span class="math inline"><em>w</em></span> augment, the area grows faster than the facade wall length. The components making the facade being more expensive than the ones making the floor and ceiling, large habitats will have a higher margin than smaller ones.</p>
<h3 id="price-setting">Price-setting</h3>
<h5 id="section-14"></h5>
<p>Luxury habitats will be priced starting at a lump-sum fee of EUR 100k and at EUR 5,000 per square meter, premium habitats will be priced starting at a lump-sum fee of EUR 50k and at EUR 2,500 per square meter, and high-density habitats will be priced starting at a lump-sum fee of EUR 35k and at EUR 1,250 per square meter. Theses prices do not include land acquisition, settlement or taxes, and are base prices: they include all that is needed for the building to work, and it will always be possible for the customer to change materials or add features, options or amenities at an extra cost. To compare, current usual housing prices in BFG start at around EUR 1,300 (excluding taxes and land) for a low-tech house, and adding a little technology raises this price to around EUR 1,500 (excluding taxes and land).</p>
<table>
<thead>
<tr class="header">
<th style="text-align: left;"><strong>Surface</strong></th>
<th style="text-align: right;"><strong>Luxury</strong></th>
<th style="text-align: right;"><strong>Premium</strong></th>
<th style="text-align: right;"><strong>High-density</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">100 m²</td>
<td style="text-align: right;">EUR 600,000</td>
<td style="text-align: right;">EUR 300,000</td>
<td style="text-align: right;">EUR 160,000</td>
</tr>
<tr class="even">
<td style="text-align: left;">200 m²</td>
<td style="text-align: right;">EUR 1,100,000</td>
<td style="text-align: right;">EUR 550,000</td>
<td style="text-align: right;">EUR 285,000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">300 m²</td>
<td style="text-align: right;">EUR 1,600,000</td>
<td style="text-align: right;">EUR 800,000</td>
<td style="text-align: right;">EUR 410,000</td>
</tr>
</tbody>
</table>
<h3 id="discounts">Discounts</h3>
<h5 id="section-15"></h5>
<p>A strict no-discount policy is in application for new habitats. The only exception is the referral program, which will be set so that our customers can give their friends exclusive benefits on a new habitat purchase with their personal referral code. This program will give benefits to both the referrer and the referees, and is thus an incentive for our customers to talk about our products.</p>
<h3 id="payment-terms">Payment terms</h3>
<h5 id="section-16"></h5>
<p>Once a habitat has been fully configured by a customer on the online configurator (with or without the help of a Product Specialist), a EUR 5k non-refundable deposit is required to place the order. Our sales service then contacts the client to potentially improve the configuration and finalize the order. Payment deadlines will then comply to local regulation.</p>
<h4 id="cash-payment">Cash payment</h4>
<h5 id="section-17"></h5>
<p>Customers can pay habitats cash, but are unlikely to do so since it is usually more interesting to use debt and invest the cash to earn more than the interest that is to be paid on the mortgage. Customers contracting a loan with an independent third party are considered as part of this category.</p>
<h4 id="lease-payment">Lease payment</h4>
<h5 id="section-18"></h5>
<p>Customers who like change or see our habitats as tech products and always want to be up to date can opt for a lease payment. The habitat will always be under warranty during the duration of the contract. The leasing can be set up with our without down payment, many contract durations are available and many options can be added to the deal (such as insurance, maintenance services etc.). At the end of the contract, the customer can return the habitat or purchase it at its residual value. A customer that has returned a habitat can choose to start a new lease.</p>
<h4 id="loan-payment">Loan payment</h4>
<h5 id="section-19"></h5>
<p>In association with a third party, we are going to offer a lending service, which will be easier and faster to get in, and less expensive.</p>
<h2 id="distribution-strategy-place">Distribution strategy (Place)</h2>
<h3 id="strategies">Strategies</h3>
<h4 id="luxury-habitats-selective-distribution">Luxury habitats – Selective distribution</h4>
<h5 id="section-20"></h5>
<p>For the luxury habitats, we will apply selective distribution by working together with internationally renowned artists, architects and luxury real estate professionals, such as <a href="https://www.sothebysrealty.com/eng">Sotheby’s International Realty</a>, <a href="https://www.engelvoelkers.com/en/">Engel &amp; Völkers</a>, <a href="https://properties.lefigaro.com">Le Figaro Properties</a> etc. They will bring us potential clients so we can control the communication and make sure they get the highest quality service possible. Luxury real estate agents usually ask for a 6% commission or less.</p>
<h4 id="premium-and-high-density-habitats-exclusive-distribution">Premium and high-density habitats – Exclusive distribution</h4>
<h5 id="section-21"></h5>
<p>We will apply exclusive distribution for premium and high-density habitats, with company owned store. This will allow us to have the opportunity to educate potential customers. Store are to be positioned in high foot traffic, high visibility retail venues, like malls and shopping streets that people regularly visit in a relatively open-minded buying mood. Product Specialists will be there to interact with potential customers and have them learn about our habitats. They are not on commission and they will never pressure to buy anything. Their goal and the sole metric of their success is to have people enjoy the experience of visiting so much that they look forward to returning again.</p>
<h3 id="location-decisions">Location decisions</h3>
<h5 id="section-22"></h5>
<p>In our first phase, we are targeting Benelux, France and Germany. Once we start to gain momentum, we will initiate a sustainable expansion to North America and the rest of Europe.</p>
<h5 id="section-23"></h5>
<p>We will gradually open stores near large cities for each country we operate in. In case of Benelux, France and Germany, here are candidate cities:</p>
<ul>
<li><p><em>Belgium:</em> Brussels, Antwerp, Ghent</p></li>
<li><p><em>Netherlands:</em> Amsterdam, Eindhoven, Rotterdam, The Hague</p></li>
<li><p><em>Luxembourg:</em> Luxembourg Ville</p></li>
<li><p><em>France:</em> Paris, Lille, Bordeaux, Nantes, Toulouse, Lyon, Marseille</p></li>
<li><p><em>Germany:</em> Berlin, Munich, Frankfurt, Nuremberg, Stuttgart, Hamburg, Cologne</p></li>
</ul>
<h3 id="transport-warehousing-and-logistics">Transport, warehousing and logistics</h3>
<h5 id="section-24"></h5>
<p>For each order, electric semi trailers will be specifically arranged with all the parts needed for the assembly, the robots and any other required equipment. The placement of parts in the trailer must be carefully planned, so that each component is accessible to robots during the assembly (i.e. the assembly order must be taken into account).</p>
<h2 id="marketing-communication-strategy-promotion">Marketing communication strategy (Promotion)</h2>
<h5 id="section-25"></h5>
<p>The official language of the company is English. All our marketing will be first and primarily in English. We want to fully Americanize our marketing and to reach American Internet media first, to avoid the lack of marketing savviness of European companies communicating in German, in French etc., thus failing to reach the world.</p>
<h3 id="promotional-mix">Promotional mix</h3>
<h4 id="advertising">Advertising</h4>
<h5 id="section-26"></h5>
<p>We will make high quality video ads that will be available on YouTube, and are to be displayed on television and in theaters. In magazines and outdoors, we will have single page/panel ads, mysteriously inducing interest from the potential customer. These ads will not be explicit, and are only there to induce potential customers to visit our website. We will use social media such as Instagram and Twitter to reach a broader audience, while keeping in mind the fundamental differences between them and providing tailored content for each platform.</p>
<h4 id="personal-selling">Personal selling</h4>
<h5 id="section-27"></h5>
<p>The first clients of the luxury habitat will be selected by hand. We want to build a very close and personal relationship with them, since they are going to help us showcase our technology and products.</p>
<h4 id="sales-promotion">Sales promotion</h4>
<h5 id="section-28"></h5>
<p>We will be present in tech, art and construction fairs, including but not limited to:</p>
<p><span>2</span></p>
<ul>
<li><p><a href="https://www.batibouw.com/en">Batibouw</a></p>
<ul>
<li><p>Description: Int’l Building Trade, Renovation and Decoration Show</p></li>
<li><p>Location: Brussels, Belgium</p></li>
<li><p>Period: 10 days in February, every year</p></li>
<li><p>Volume: 300k visitors</p></li>
</ul></li>
<li><p><a href="https://www.ces.tech">International CES</a></p>
<ul>
<li><p>Description: Source for Consumer Technologies</p></li>
<li><p>Location: Las Vegas, USA</p></li>
<li><p>Period: 3 days in January, every year</p></li>
<li><p>Volume: 185k visitors</p></li>
</ul></li>
</ul>
<h5 id="section-29"></h5>
<p>Our stand will be adapted to the theme of the event. An habitat will be assembled and disassembled non-stop, showing current info about the assembly and displaying the amount of assembly cycles since the beginning of the event. Next to it will be an assembled habitat, showcasing different finishing surfaces and different layouts. Numerous features of the habitat are also to be demoed, such as the luminous ceiling or the power doors.</p>
<h4 id="public-relations">Public relations</h4>
<h5 id="section-30"></h5>
<p>We are to invite the press to write articles about us. We mainly target specialized press in the fields of tech (such as <a href="https://www.wired.com">WIRED</a>, <a href="https://www.theverge.com">The Verge</a>, <a href="https://www.wsj.com/">The Wall Street Journal</a>, <a href="https://www.cnet.com">CNET</a>, <a href="https://techcrunch.com">TechCrunch</a> etc.), art, architecture (such as <a href="https://www.archdaily.com">ArchDaily</a>) and luxury items. We are also targeting the press that is read by the higher income layer of the population. We will also do talks in seminars (such as <a href="https://www.ted.com/">TED Talks</a> and <a href="https://events.recode.net/events/code-conference-2018/">Code Conference</a>) to educate about our technology and products. We will invite major social media influencers (such as <a href="http://mkbhd.com">Marques Brownlee (MKBHD)</a>, <a href="https://www.caseyneistat.com">Casey Neistat</a>, <a href="https://www.recode.net/authors/kara-swisher">Recode’s Kara Swisher</a>, <a href="https://hyperchangetv.com/pages/about">HyperCHANGE’s Galileo Russel</a>, <a href="https://answerswithjoe.com">Answers with Joe’s Joe Scott</a>, <a href="https://electrek.co/author/fredericclambert/">Electrek’s Fred Lambert</a>, <a href="http://www.fullychargedshow.co.uk">Fully Charged’s Robert Llewellyn</a>, <a href="https://www.theverge.com/walt-mossberg-verge">The Verge’s Walt Mossberg</a>, <a href="https://waitbutwhy.com/wait-but-who">Wait But Why’s Tim Urban</a> etc.) to visit and review our products. We want honest reviews.</p>
<h5 id="section-31"></h5>
<p>We want to make a product that people love. If they really love it, they will talk about it. We will introduce a referral program that will be an incentive to buy our product (with benefits for both the referrer and the referees).</p>
<h4 id="direct-marketing">Direct marketing</h4>
<h5 id="section-32"></h5>
<p>We will organize keynotes in which we will convey the press, those events will be live streamed, and there will be prototypes to visit after the presentation. Our website will educate people about what we do, with compelling information and graphics.</p>
<h3 id="message-strategy">Message strategy</h3>
<h5 id="section-33"></h5>
<p>In the case of ads and product placements, we will focus on showing life moments of different types of people (complete families, young, old etc. with adapted music, decor, activities etc.) in the habitat without talking directly about the product. The subjects will look successful and fulfilled, and the message will be that in our habitat they feel safe and comfortable, they can focus on what really matters. One hidden message will be that successful people live in our habitats, and so if you live in our habitats, you will be successful.</p>
<h5 id="section-34"></h5>
<p>When we are to talk explicitly about Ark Habitat, we focus on safety, design, seamless integration of technology and innovation. We need to talk to the reptilian brain of the people, let them know their life will be better in one of our habitats. We want to reach both their rationale and emotions.</p>
<h5 id="section-35"></h5>
<p>Some of our ads will also target tech savvy people: for example, we will make an ad showing high paced robots dancing while assembling a habitat, and an ad in which a smaller robot will be personified and shown as curious during an assembly, but must get back to work since a larger robot has seen him being distracted.</p>
<h3 id="message-frequency">Message frequency</h3>
<h5 id="section-36"></h5>
<p>Once at cruising speed, we plan to have 2 to 4 keynotes a year, to release new versions of the software and hardware. Ads and other types of promotion will follow those events.</p>
<h2 id="sales-and-marketing-forecasts">Sales and Marketing Forecasts</h2>
<h5 id="section-37"></h5>
<p>The following sales forecasts are taking to account sales in the BFG zone, and the following expansion to the rest of the European and North American markets.</p>
<h3 id="sales-projection-by-year">Sales projection by year</h3>
<h3 id="sales-projection-by-quarter">Sales projection by quarter</h3>
<h3 id="methodology">Methodology</h3>
<h5 id="section-38"></h5>
<p>Our sales model is based on the build-up method, due to the disruptive nature of our business. The sales ramp up can be modelled by a generalized logistic function (a flexible S-shaped curve) that is given by the following equation: <span class="math display"><em>Y</em>(<em>t</em>) = <em>A</em></span></p>
<h5 id="section-39"></h5>
<p>Where <span class="math inline"><em>Y</em></span> is the production volume and <span class="math inline"><em>t</em></span> is the time. It has 6 parameters:</p>
<ul>
<li><p><span class="math inline"><em>A</em></span>: the lower asymptote;</p></li>
<li><p><span class="math inline"><em>K</em></span>: the upper asymptote. If <span class="math inline"><em>A</em> = 0</span> then <span class="math inline"><em>K</em></span> is called the carrying capacity;</p></li>
<li><p><span class="math inline"><em>B</em></span>: the growth rate;</p></li>
<li><p><span class="math inline"><em>ν</em> &gt; 0</span>: affects near which asymptote maximum growth occurs;</p></li>
<li><p><span class="math inline"><em>Q</em></span>: is related to the value <span class="math inline"><em>Y</em>(0)</span>;</p></li>
<li><p><span class="math inline"><em>C</em></span>: typically takes a value of 1;</p></li>
<li><p><span class="math inline"><em>M</em></span>: starting time <span class="math inline"><em>t</em><sub>0</sub></span>.</p></li>
</ul>
<h5 id="section-40"></h5>
<p>The logistic, with maximum growth rate at time <span class="math inline"><em>M</em></span>, is the case where <span class="math inline"><em>Q</em> = <em>ν</em> = 1</span>.</p>
<h5 id="section-41"></h5>
<p>The rules to establish those parameters for luxury, premium and high-density habitats were the following:</p>
<ul>
<li><p><span class="math inline"><em>A</em> = 0</span> since the starting production volume is <span class="math inline">0</span>;</p></li>
<li><p>The carrying capacity <span class="math inline"><em>K</em></span> is calculated by taking the total amount of new single-family homes built every year in BFG (51k in Benelux, 200k in France and 100k in Germany), select their corresponding tranches (resp. top 0.07%, 5% and 40% for luxury, premium and high-density habitats) and choose our target market share at cruising speed (resp. 20%, 15% and 10% for luxury, premium and high-density habitats);</p></li>
<li><p>The growth rate <span class="math inline"><em>B</em></span> is calculated based on the expected time to maturation, which is the time it would take to reach more than 95% of our target market share (resp. 5, 10 and 20 years for luxury, premium and high-density habitats). Growth rates are individually adjusted so that the ramp up initial speed is not too high, and the logistic curve fills the time to maturation;</p></li>
<li><p>The starting time <span class="math inline"><em>M</em></span> is equal to half the time to maturation plus the production start time;</p></li>
<li><p><span class="math inline"><em>ν</em></span>, <span class="math inline"><em>Q</em></span> and <span class="math inline"><em>C</em></span> are set to resp. <span class="math inline">0.5</span>, <span class="math inline">0.5</span> and <span class="math inline">1</span> for all habitat variants.</p></li>
</ul>
<table>
<caption>Generalized logistic function parameter values for luxury, premium and high-density habitats in BFG.</caption>
<thead>
<tr class="header">
<th style="text-align: left;"><strong>Parameter</strong></th>
<th style="text-align: left;"><strong>Luxury</strong></th>
<th style="text-align: left;"><strong>Premium</strong></th>
<th style="text-align: left;"><strong>High-density</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"><span class="math inline"><em>A</em></span></td>
<td style="text-align: left;">0</td>
<td style="text-align: left;">0</td>
<td style="text-align: left;">0</td>
</tr>
<tr class="even">
<td style="text-align: left;"><span class="math inline"><em>K</em></span></td>
<td style="text-align: left;">50</td>
<td style="text-align: left;">2,633</td>
<td style="text-align: left;">14,040</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><span class="math inline"><em>B</em></span></td>
<td style="text-align: left;">100%</td>
<td style="text-align: left;">100%</td>
<td style="text-align: left;">100%</td>
</tr>
<tr class="even">
<td style="text-align: left;"><span class="math inline"><em>ν</em></span></td>
<td style="text-align: left;">0.5</td>
<td style="text-align: left;">0.5</td>
<td style="text-align: left;">0.5</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><span class="math inline"><em>Q</em></span></td>
<td style="text-align: left;">0.5</td>
<td style="text-align: left;">0.5</td>
<td style="text-align: left;">0.5</td>
</tr>
<tr class="even">
<td style="text-align: left;"><span class="math inline"><em>C</em></span></td>
<td style="text-align: left;">1</td>
<td style="text-align: left;">1</td>
<td style="text-align: left;">1</td>
</tr>
<tr class="odd">
<td style="text-align: left;"><span class="math inline"><em>M</em></span></td>
<td style="text-align: left;">4.5</td>
<td style="text-align: left;">7.5</td>
<td style="text-align: left;">11.5</td>
</tr>
</tbody>
</table>
<h5 id="section-42"></h5>
<p>We also have taken into account some global economic factors:</p>
<section class="footnotes" role="doc-endnotes">
<hr />
<ol>
<li id="fn1" role="doc-endnote"><p>Consumer surplus: difference between the maximum price a consumer is willing to pay and the actual price they do pay<a href="#fnref1" class="footnote-back" role="doc-backlink">↩︎</a></p></li>
</ol>
</section>
</body>
</html>
