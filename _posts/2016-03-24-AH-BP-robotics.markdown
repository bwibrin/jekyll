---
layout: post
title: "Latex Markdown Post"
date: 2016-03-24 15:32:14 -0300
categories: jekyll update
---

# Assembly with Cranebots and AGVs

Ark Habitat aims to revolutionize the way we build habitats by
introducing unprecedented levels of automation on the construction site.
The assembly system that we have elaborated rely on the use of two main
automated machines:

- **Cranebots** that will be the first automated and intelligent
  cranes ever developed, whose task will be to lift the SPUs and
  assemble them with gravity lock. The exact name for this type of
  robot is a \"cylindrical manipulator with spherical wrist\". We are
  not aware of the existence of those cranebots, such that we will
  need to develop this new system. These machines will have to be
  lightweight, easily deployed on site, achieve a high level of
  precision (1 mm) and be able to communicate and synchronize with the
  other machines on site.

- **AGVs** or Automated Guided Vehicles operating inside the habitat
  and designed to execute various tasks such as fixing (drilling) the
  SPUs placed by the cranebots, carry and assemble lightweight
  elements (such as the finishing surfaces) and execute various
  finishing works.

## Technical requirements

Before moving to the development plan, let's first list the technical
requirements of the aforementioned machines, in order to estimate the
development needs.

### Cranebot

The cranebot is an automated crane at the end of which a robotic arm is
mounted. This completely innovative system, that will be patented, will
be capable of lifting moderate loads inferior to 500 kg and reach a
level of precision of 1 mm.

The cranebot will be transportable in a truck container (12m x

### AGVs

The new construction technology that we are developing will require the
use of specific AGVs on which a robotic arm will be mounted in order to
execute lightweight and precision tasks from inside the habitat, such
as:

- fixing the SPUs that have been gravity-locked by the cranebots
  (drilling)

- carry and fixing lightweight elements (\< 60 kg) such as finishing
  surface (Kerlite for instance)

- executing various finishing works

Multiple AGVs will hence be deployed on the ground plane (previously
laid down by cranebots) and collaborate (between them as well as with
the cranebots) to reach the highest level of efficiency.

Such a specific AGV will require a development with specialized AGV and
robotic firms. We have found some examples of such AGVs, but only in a
limited number, and at what still looks like a prototype stage:

- <http://www.opiflex.se/en/mobile-robot-platform/>

- <https://www.logistiek.nl/warehousing/nieuws/2017/03/dematic-lanceert-agv-met-robotarm-101153468>

- In situ fabricator (ETH)
  <https://www.youtube.com/watch?v=TCJOQkOE69s>

#### Desired technical specifications

- Size: Lenght \< 2000 mm / Width \< 1 m (to pass through doors) /
  Height \< 2000 mm high (to pass through doors)

- Weight: \< 2000 kg (as low as possible)

- Battery: at least 8 hours of continuous operation / 1 hour Wifi for
  full recharge

- Payload: max 60 kg Range: 2.50 m (to reach the next platform)

- Max velocity of the base: around 1 m/s

- Precision of the end effector: 0.05 mm

- Work environment: capable to operate in exterior conditions

  - Temperature: -5°C to 45°C (cold is more of a concern)

  - Dust

  - Wind

- Security: capable of collaboration with humans (slow down if someone
  gets closer, and stops if too close), able to avoid collision with
  unexpected obstacles.

- Design look

The max payload has been computed on basis of Kerlite density (8 kg/m²
for a width of 5 mm) and for a which a 2 m x 3 m finishing surface plate
would weight 50 kg. For heavier materials (marble for instance), we will
potentially have to develop another class of dedicated AGV capable of
lifting up to 100 kg. It is important that the robotic AGV are able to
lift all the finishing surfaces and inner walls of the habitat, in order
to automatize also the upgradability (change of finishing surfaces,
modification of the layout of the rooms, \...). Of course, heavier
elements could be lifted by the cranebot, but this would not be possible
in case of interior works for upgrade.

#### Achievable technical specifications based on the market

Let's try to estimate the technical specifications that seem achievable
based on what can be found on the AGV market at the moment. The
estimation are based on the Kuka KMP1500 for the AGV platform
<https://www.kuka.com/en-de/products/mobility/mobile-platforms/kmp-1500>
and the ABB IRB 4600 as robotic arm ( OpiFlex MRP60 uses such a robotic
arm on a docking session
<http://www.opiflex.se/en/mobile-robot-platform/>, but no spec sheet
available).

²

- Kuka KMP1500:

  - Size: 2000mm long - 800mm wide - 470mm high

  - Weight: 1000 kg

  - Battery: 104 Ah / 96 V / 10 kWh / Charging time: 2h (5kW)

  - Payload: 1500 kg

  - Max velocity: 1 m/s

  - Precision: 5 mm

  - Temperature: ?

- Robotic arm ABB IRB4600:

  - Manipulator: ABB IRB4600

  - Control System: ABB IRC5

  - Size : circa 1600 mm long - 700 mm wide - min 600 mm high (when
    extended horizontally) max 3000 mm (when extended vertically)
    and circa 2000 mm in home position

  - Weight: 425 kg + 30 kg controller

  - Range: 2.55 m

  - Payload: 60 kg

  - Power consumption: max 1.5 kW with max payload

  - Repeated Precision Docking: 0.05 mm

  - Temperature: +5°C to +45°C (can go up to 70°C during less than
    24 hours, warm-up needed if below 0°C)

Based on those figures, we derived the following technical specs for our
integrated AGV with robotic arm solution, or AGR (Automated Guided
Robot):

- Size: 2000 mm long - 800 mm wide - 2500 mm high (robot in home
  position + AGV height) capable of going through doors if extended
  horizontally

- Weight: max 1500 kg (1000 kg AGV + 500 kg robot)

- Battery: 25 kWh for at least 8 hours operation (10 kWh AGV + 15 kWh
  robot) / 1 hour Wifi charging (in2Source)

- Payload: 60 kg

- Max velocity of the base: 1 m/s

- Precision: 0.05 mm (with compensation for the exact position of the
  AGV)

- Guidance system: laser guidance technology (LGV for laser guided
  vehicle)

- Temperature: +5°C to +45°C (can go up to 70°C during less than 24
  hours, warm-up needed if below 0°C)

Regarding the guidance system, we would a priori use laser guidance
technology (LGV for laser guided vehicle) as it is the most accurate,
reliable and flexible system according to
<https://www.transbotics.com/learning-center/guidance-navigation>.

## Contacts

### AGVs

Belgium:

- <https://www.dematic.com/en-us/>

- <https://www.scottautomation.com/contact/>

- <http://www.motum.be/>

- <http://www.axter-agv.com/contact/> (belgian antenna in Saintes)

- <https://in2power.be> (for wireless charging solutions)

- <https://www.enovates.com/> (for wireless charging solutions)

### Metrology

Belgium:

- <https://leica-geosystems.com/> (Diegem)

### Cranes

Belgium:

- <https://www.liebherr.com/en/int/about-liebherr/liebherr-worldwide/belgium/liebherr-in-belgium.html>
  (Merksem)

- <https://www.aertssen.be/> (Anvers)

- <https://nl.konecranes.be/> (Floreffe)

- <https://www.deman.be/en/contact>

- <https://www.mennensbelgium.be/en/contact-us> zwijndrecht

- <https://www.abuscranes.com/contact> Liège

Electrical cranes:

- <https://www.jmgcranes.it/> (Italy)

- <http://www.ble.be/en> distributors of electric cranes JMG

Tailor made crane design:

- <http://crane-design.net/for-new-clients.html>

### Other links

- <http://www.system-agv.com/>

- <https://www.bastiansolutions.com/>
